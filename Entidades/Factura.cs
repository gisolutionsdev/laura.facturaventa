﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Factura
    {
        public string NroDcto  { get; set; }

        public DateTime? Fecha { get; set; }

        public DateTime? FechaVence { get; set; }

        public string Nombre { get; set; }

        public string Nit { get; set; }

        public string Direccion { get; set; }

        public string DireccionAlterna { get; set; }

        public string Pais { get; set; }

        public string Ciudad { get; set; }

        public string Telefono { get; set; }

        public string CodigoCliente { get; set; }

        public string NroPedido { get; set; }

        public string NroOrden { get; set; }

        public string Zona { get; set; }

        public string Transportador { get; set; }

        public string Vendedor { get; set; }

        public string CondicionesPago { get; set; }

        public string CorreoCliente { get; set; }

        public string Referencia { get; set; }

        public string Descripcion { get; set; }

        public string DescripcionCorta { get; set; }

        public decimal? Cantidad { get; set; }

        public string UnidadMedida { get; set; }

        public decimal? VlrUnit { get; set; }

        public decimal? VlrUnitTotal { get; set; }

        public int TotalGrupo { get; set; }

        public int TotalItems { get; set; }

        public string Agrupacion { get; set; }

        public decimal? Bruto { get; set; }

        public decimal? BrutoDolar { get; set; }

        public decimal? Descuento { get; set; }

        public decimal? Iva { get; set; }

        public decimal? Retefuente { get; set; }

        public decimal? VlrNetoPagar { get; set; }

        public string  UsuOfima { get; set; }

        public string ValoresLetra { get; set; }

        public decimal? PesoNeto { get; set; }

        public decimal? PesoBruto { get; set; }

        public int? TotalCajas { get; set; }

        public string DsctoComercial { get; set; }

        public string LugarFob { get; set; }

        public string FechaFob { get; set; }

        public string Nota { get; set; }

        public bool Activo { get; set; }

        public string CodAlterno { get; set; }
    }
}
