﻿using Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacturaVenta
{
    public partial class FrmPopUp : Form
    {
        private List<TiposDctos> lstDocumentos;
        public string pCodigo = string.Empty;
        public string pDescripcion = string.Empty;

        public FrmPopUp(List<TiposDctos> documentos)
        {
            InitializeComponent();
            lstDocumentos = documentos;
            dgvDatos.AutoGenerateColumns = false;
            dgvDatos.DataSource = lstDocumentos;

        }

        public void Seleccionar()
        {
            if (dgvDatos.SelectedRows.Count > 0)
            {
                pCodigo = dgvDatos.Rows[dgvDatos.SelectedRows[0].Index].Cells[0].Value.ToString();
                pDescripcion = dgvDatos.Rows[dgvDatos.SelectedRows[0].Index].Cells[1].Value.ToString();
                this.Close();
            }
        }

        private void txtFiltro_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Seleccionar();
            }
            else if (e.KeyCode == Keys.Down)
            {
                if (dgvDatos.Rows.Count > 0 && dgvDatos.SelectedRows[0].Index < dgvDatos.Rows.Count - 1)
                {
                    dgvDatos.Rows[dgvDatos.SelectedRows[0].Index + 1].Selected = true;
                    dgvDatos.CurrentCell = dgvDatos.SelectedRows[0].Cells[0];
                }
            }
            else if (e.KeyCode == Keys.Up)
            {
                if (dgvDatos.Rows.Count > 0 && dgvDatos.SelectedRows[0].Index > 0)
                {
                    dgvDatos.Rows[dgvDatos.SelectedRows[0].Index - 1].Selected = true;
                    dgvDatos.CurrentCell = dgvDatos.SelectedRows[0].Cells[0];
                }
            }
        }

        private void dgvDatos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Seleccionar();
            }
        }

        private void FrmPopUp_Load(object sender, EventArgs e)
        {

        }

        private void btnSeleccionar_Click_1(object sender, EventArgs e)
        {
            Seleccionar();
        }

        private void txtFiltro_TextChanged_1(object sender, EventArgs e)
        {
            string filtro = txtFiltro.Text.ToLower().Trim();
            dgvDatos.DataSource = lstDocumentos.Where(x => x.Codigo.ToLower().Contains(filtro) || x.Descripcion.ToLower().Contains(filtro)).ToList();
        }
    }
}

