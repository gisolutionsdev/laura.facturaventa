﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacturaVenta
{
    public partial class FrmPreview : Form
    {
        FrmPrincipal Reporte;
        public FrmPreview(FrmPrincipal Reporte)
        {
            InitializeComponent();
            this.Reporte = Reporte;
        }

        private void FrmPreview_Load(object sender, EventArgs e)
        {
            this.PreviewReport.Refresh();
            this.PreviewReport.ZoomMode = Microsoft.Reporting.WinForms.ZoomMode.PageWidth;
        }

        private void FrmPreview_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}
