﻿using Datos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class FacturaRepository
    {
        private CompanyEntities _Context;
        public string CadenaConexion;
        SqlConnection Ocon;

        public DataTable ConsultaDeFacturaVenta(string nrodcotini, string nrodctofin, string tipodcto, string palabrauno, string palabrados, string palabratres, string palabracuatro)
        {

            try
            {
                CadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["Conexion"].ToString();
                SqlCommand oCom = new SqlCommand();

                SqlParameter[] Parametros =
                {
                    new SqlParameter("@DCTOINICIAL",SqlDbType.Char,20) {Value = nrodcotini },
                    new SqlParameter("@DCTOFINAL",SqlDbType.Char,20) {Value = nrodctofin },
                    new SqlParameter("@TIPODCTO", SqlDbType.Char,2) {Value = tipodcto },
                    new SqlParameter("@PALABRAUNO", SqlDbType.Char,20) {Value = palabrauno},
                    new SqlParameter("@PALABRADOS", SqlDbType.Char,20) {Value = palabrados},
                    new SqlParameter("@PALABRATRES", SqlDbType.Char,20) {Value = palabratres},
                    new SqlParameter("@PALABRACUATRO", SqlDbType.Char,20) {Value = palabracuatro},
                };

                Ocon = new SqlConnection(CadenaConexion);
                Ocon.Open();


                oCom.CommandType = System.Data.CommandType.StoredProcedure;
                oCom.CommandText = "SP_FACTURACOMPOSICION";
                oCom.Parameters.AddRange(Parametros);
                SqlDataAdapter oAdp = new SqlDataAdapter();
                DataTable oDt = new DataTable();

                oCom.Connection = Ocon;
                oAdp.SelectCommand = oCom;
                oAdp.Fill(oDt);

                return oDt;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public DataTable ConsultaDeFacturaVentaProducto(string tipodcto, int nrodcotini, int nrodctofin)
        {
            CadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["Conexion"].ToString();
            object[] Parametros = new object[3];
            Parametros[0] = tipodcto;
            Parametros[1] = nrodcotini;
            Parametros[2] = nrodctofin;

            Ocon = new SqlConnection(CadenaConexion);
            Ocon.Open();

            SqlCommand oCom = new SqlCommand();
            oCom.CommandType = System.Data.CommandType.StoredProcedure;
            oCom.CommandText = "";
            oCom.Parameters.Add(Parametros);
            SqlDataAdapter oAdp = new SqlDataAdapter();
            DataTable oDt = new DataTable();

            oCom.Connection = Ocon;
            oAdp.SelectCommand = oCom;
            oAdp.Fill(oDt);

            return oDt;
        }

        public DataTable ConsultaDeFacturaVentaProductoP()
        {
            CadenaConexion = System.Configuration.ConfigurationManager.ConnectionStrings["Conexion"].ToString();


            Ocon = new SqlConnection(CadenaConexion);
            Ocon.Open();

            SqlCommand oCom = new SqlCommand();
            oCom.CommandType = System.Data.CommandType.StoredProcedure;
            oCom.CommandText = "SP_FACTURACOMPOSICION";

            SqlDataAdapter oAdp = new SqlDataAdapter();
            DataTable oDt = new DataTable();

            oCom.Connection = Ocon;
            oAdp.SelectCommand = oCom;
            oAdp.Fill(oDt);

            return oDt;
        }
    }
}
