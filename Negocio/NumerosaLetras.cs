﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Negocio
{
    public class NumerosaLetras
    {
        private readonly static string[] _numbersInWords = {

            "uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve",

            "diez", "once", "doce", "trece", "catorce", "quince", "dieciseis", "diecisiete", "dieciocho", "diecinueve",

            "veinte", "veintiuno", "veintidos", "veintitres", "veinticuatro", "veinticinco", "veintiseis", "veintisiete", "veintiocho", "veintinueve",

            "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa",

                  "ciento", "doscientos", "trescientos", "cuatrocientos", "quinientos", "seiscientos", "setecientos", "ochocientos", "novecientos"

            };



        private const string _centWord = "centavo(s)";

        private const string _unMillonWord = "un millon";

        private const string _millonesWord = "millones";

        private const string _unMilWord = "un mil";

        private const string _milWord = "mil";

        private const string _CienWord = "cien";

        private const string _CientoUnWord = "ciento un";

        private const string _zeroWithWord = "cero con";

        private const string _withWord = "con";

        private const string _andWord = "y";



        /// <summary>

        /// Converts numbers to words

        /// </summary>

        /// <param name="number"></param>

        /// <returns></returns>

        public static string NumberToWords(decimal number, string Moneda)
        {
            if (number > 999999999999)

                return "Numero demasiado grande";




            StringBuilder Words;

            string FormattedNumberString;

            int decimalSeparatorLocation;

            int milmillones;

            int millones;

            int miles;

            int cientos;

            int centavos;

            int hundreds;

            int tens;

            int units;

            int NumeroActual = 0;



            Words = new StringBuilder();

            FormattedNumberString = number.ToString("000000000000.00");

            char DecimalSeparator = Convert.ToChar((Convert.ToString(1.1)).Trim().Substring(1, 1));

            decimalSeparatorLocation = FormattedNumberString.IndexOf(DecimalSeparator);

            milmillones = Convert.ToInt32(FormattedNumberString.Substring(0, 3));

            millones = Convert.ToInt32(FormattedNumberString.Substring(3, 3));

            miles = Convert.ToInt32(FormattedNumberString.Substring(6, 3));

            cientos = Convert.ToInt32(FormattedNumberString.Substring(9, 3));

            centavos = Convert.ToInt32(FormattedNumberString.Substring(decimalSeparatorLocation + 1, 2));

            for (int NumberPart = 1; NumberPart <= 5; NumberPart++)
            {

                switch (NumberPart)
                {
                    case 1:
                        {
                            NumeroActual = milmillones;
                            if (milmillones == 1)
                            {
                                Words.Append(_milWord);
                                Words.Append(' ');
                                continue;
                            }

                            break;
                        }
                    case 2:
                        {
                            NumeroActual = millones;

                            if (milmillones != 1 && milmillones != 0)
                            {
                                if (millones == 0)
                                    Words.Append(_milWord + " millones");
                                else
                                    Words.Append(_milWord);
                                Words.Append(' ');
                            }

                            if (millones == 1)
                            {
                                Words.Append(_unMillonWord);
                                Words.Append(' ');
                                continue;
                            }

                            break;
                        }

                    case 3:
                        {
                            NumeroActual = miles;

                            if (millones != 1 && millones != 0)
                            {
                                Words.Append(_millonesWord);
                                Words.Append(' ');
                            }

                            if (miles == 1)
                            {
                                Words.Append(_unMilWord);
                                Words.Append(' ');
                                continue;
                            }
                            break;
                        }
                    case 4:
                        {
                            NumeroActual = cientos;
                            if (miles != 1 && miles != 0)
                            {
                                Words.Append(_milWord);
                                Words.Append(' ');
                            }
                            break;
                        }
                    case 5:
                        {
                            NumeroActual = centavos;
                            if (centavos != 0)
                            {
                                if (millones == 0 && miles == 0 && cientos == 0)
                                {
                                    Words.Append(_zeroWithWord);
                                    Words.Append(' ');
                                }
                                else
                                {
                                    Words.Append(Moneda);
                                    Words.Append(' ');
                                    Words.Append(_withWord);
                                    Words.Append(' ');
                                }
                            }
                            break;
                        }
                }

                hundreds = (int)(NumeroActual / 100);
                tens = (int)(NumeroActual - hundreds * 100) / 10;
                units = (int)(NumeroActual - (hundreds * 100 + tens * 10));

                if (NumeroActual == 0) continue;

                if (NumeroActual == 100)
                {
                    Words.Append(_CienWord);
                    Words.Append(' ');
                    continue;

                }

                else
                {

                    if (NumeroActual == 101 && NumberPart != 3)
                    {
                        Words.Append(_CientoUnWord);
                        Words.Append(' ');
                        continue;
                    }

                    else
                    {
                        if (NumeroActual > 100)
                        {
                            Words.Append(_numbersInWords[hundreds + 35]);
                            Words.Append(' ');
                        }
                    }
                }

                if (tens < 3 && tens != 0)
                {
                    Words.Append(_numbersInWords[tens * 10 + units - 1]);
                    Words.Append(' ');
                }

                else
                {
                    if (tens > 2)
                    {
                        Words.Append(_numbersInWords[tens + 26]);
                        Words.Append(' ');

                        if (units == 0)
                        {
                            continue;
                        }

                        Words.Append(_andWord);
                        Words.Append(' ');
                        Words.Append(_numbersInWords[units - 1]);
                        Words.Append(' ');
                    }

                    else
                    {
                        if (tens == 0 && units != 0)
                        {
                            Words.Append(_numbersInWords[units - 1]);
                            Words.Append(' ');
                        }
                    }
                }
            } // end for

            if (centavos != 0)
                Words.Append(_centWord);
            else
                Words.Append(Moneda);

            // Resolve particular problems here.

            Words.Replace("uno mil", "un mil");

            return Words.ToString().Trim().Replace("  ", " ");
        }
    }
}
