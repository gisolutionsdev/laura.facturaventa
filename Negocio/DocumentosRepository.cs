﻿using Datos;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class DocumentosRepository
    {
        private CompanyEntities _Context;

        public List<TiposDctos> ListarTiposDoc()
        {
            try
            {
                using (_Context = new CompanyEntities())
                {
                    var res = from td in _Context.TIPODCTO
                              where td.ORIGEN == "FAC"
                              select new TiposDctos { Codigo = td.TIPODCTO1, Descripcion = td.DESCRIPCIO };
                    return res.ToList();
                }
            }
            catch (Exception E)
            {

                throw;
            }
        }

        public string Consecutivo(string TipoDoc)
        {

            try
            {
                using (_Context = new CompanyEntities())
                {
                    var res = from c in _Context.CONSECUT
                              where c.TIPODCTO == TipoDoc
                              select c.CONSECUT1;
                    return res.FirstOrDefault().ToString();
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
