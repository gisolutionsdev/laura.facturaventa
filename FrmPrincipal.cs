﻿using Entidades;
using Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace FacturaVenta
{
    public partial class FrmPrincipal : Form
    {
        #region Parametros

        string Copia;
        string Palabrauno = string.Empty;
        string PalabraDos = string.Empty;
        string PalabraTres = string.Empty;
        string PalabraCuatro = string.Empty;
        string CorNacional;
        string NombreEmpresa;
        string Nit;
        string Tel;
        string Dir;
        string Apartado;
        string TextoLetraCambio;
        string Regimen;
        string Cor;
        string Web;
        string Observaciones;
        string DeclaracionFOB;
        string TextoFOB;
        string EmpresaFOB;
        string AutoRete;
        string ResAutorete;
        string ActEconomica;
        string TextoAutorete;
        string ResDian;
        string PagoBanco;
        string PagoCheque;
        string Logo;
        string Nota;
        static LocalReport reporte;
        string DocInicial;
        string DocFinal;
        string TipoDcto;
        string Impresion;
        string Zona;
        string CodZona1;
        string CodZona2;
        string CodZona3;
        string CodZona4;
        string CodZona5;
        string CodZona6;
        string Cliente;
        string CodCliente1;
        string CodCliente2;
        List<Factura> lstFacturaVenta = new List<Factura>();
        private List<Stream> m_streams;
        private int m_currentPageIndex;
        LocalReport RptPrev = null;
        FrmPreview frmPrev = null;

        #endregion

        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            ListarImpresoras();
            CargarDocumentos();
            chkDeProducto.Checked = true;
            grbParamFactPorducto.Enabled = true;
            chkFactProductoPesos.Checked = true;
            chkImprimir.Checked = true;
        }

        #region Validaciones
        public bool ValidarCampos()
        {

            if ((!chkDeProducto.Checked) && (!chkFacComposicion.Checked))
            {
                MessageBox.Show("Debe Seleccionar un tipo de Factura ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            if ((string.IsNullOrEmpty(txtTipoDcto.Text)) || (string.IsNullOrEmpty(txtConsecutivoInicial.Text)) || (string.IsNullOrEmpty(txtConsecutivoFinal.Text)))
            {
                MessageBox.Show("Debe Seleccionar un tipo documento o un consecutivo inicial y final ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            if (chkFacComposicion.Checked)
            {
                if ((!chkIdiomaEspanol.Checked) && (!chkIdiomaIngles.Checked))
                {
                    MessageBox.Show("Debe Seleccionar un Idioma ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }

            }

            if (string.IsNullOrEmpty(cmbImpresoras.Text))
            {
                MessageBox.Show("Debe seleccionar una Impresora ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            if (string.IsNullOrEmpty(nudCopias.Text))
            {
                MessageBox.Show("Debe indicar la cantidad de copias ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            return true;
        }

        private void chkFacComposicion_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFacComposicion.Checked)
            {
                chkDeProducto.Checked = false;
                grbParametrosComposicion.Enabled = true;
                grbParamFactPorducto.Enabled = false;
                grbParametrosCopiaCartera.Enabled = false;
                chkIdiomaEspanol.Checked = true;
                nudCopias.Value = 4;
                chkCopiaCartera.Checked = false;
                chkCopiaCartera.Enabled = false;
            }
        }

        private void chkDeProducto_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDeProducto.Checked)
            {
                chkFacComposicion.Checked = false;
                grbParametrosComposicion.Enabled = false;
                grbParamFactPorducto.Enabled = true;
                grbParametrosCopiaCartera.Enabled = true;
                chkFactProductoPesos.Checked = true;
                nudCopias.Value = 2;
                chkCopiaCartera.Enabled = true;
            }
        }

        private void chkIdiomaEspanol_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIdiomaEspanol.Checked)
            {
                chkIdiomaIngles.Checked = false;
            }
        }

        private void chkIdiomaIngles_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIdiomaIngles.Checked)
            {
                chkIdiomaEspanol.Checked = false;
            }
        }

        private void chkImprimir_CheckedChanged(object sender, EventArgs e)
        {
            if (chkImprimir.Checked)
            {
                chkPreview.Checked = false;
                txtConsecutivoFinal.Enabled = true;
                txtConsecutivoFinal.Text = txtConsecutivoInicial.Text;
                btnImprimir.Text = "Imprimir";
            }
        }

        private void chkPreview_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPreview.Checked)
            {
                chkImprimir.Checked = false;
                txtConsecutivoFinal.Enabled = false;
                txtConsecutivoFinal.Text = txtConsecutivoInicial.Text;
                btnImprimir.Text = "Previzualización";
            }
        }

        private void chkFactProductoPesos_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFactProductoPesos.Checked)
            {
                chkFacProductoDolares.Checked = false;
            }
        }

        private void chkFacProductoDolares_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFacProductoDolares.Checked)
            {
                chkFactProductoPesos.Checked = false;
            }
        }

        private void chkCopia_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCopia.Checked)
            {
                chkCopiaCartera.Checked = false;
                nudCopias.Value = 1;
            }
        }

        private void chkCopiaCartera_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCopiaCartera.Checked)
            {
                chkCopia.Checked = false;
                nudCopias.Value = 2;
            }
        }

        #endregion

        #region Metodos
        private void CargarDocumentos()
        {
            List<TiposDctos> lstDocumentos = new DocumentosRepository().ListarTiposDoc();
            txtTipoDcto.Text = "FA";
            txtConsecutivoInicial.Text = new DocumentosRepository().Consecutivo(txtTipoDcto.Text);
            txtConsecutivoFinal.Text = txtConsecutivoInicial.Text;

        }

        private void ListarImpresoras()
        {
            cmbImpresoras.Items.Clear();
            foreach (string p in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                cmbImpresoras.Items.Add(p);
            }
            System.Drawing.Printing.PrinterSettings settings = new System.Drawing.Printing.PrinterSettings();
            // Establece impresora predeterminada
            cmbImpresoras.Text = settings.PrinterName;
        }

        public void CargarParametros()
        {
            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("NombreEmpresa-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'NombreEmpresa', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                NombreEmpresa = System.Configuration.ConfigurationManager.AppSettings[string.Format("NombreEmpresa-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("Nit-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'Nit', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Nit = System.Configuration.ConfigurationManager.AppSettings[string.Format("Nit-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("Dir-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'Dir', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Dir = System.Configuration.ConfigurationManager.AppSettings[string.Format("Dir-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("Apartado-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'Apartado', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Apartado = System.Configuration.ConfigurationManager.AppSettings[string.Format("Apartado-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("Tel-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'Tel', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Tel = System.Configuration.ConfigurationManager.AppSettings[string.Format("Tel-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("Web-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'Web', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Web = System.Configuration.ConfigurationManager.AppSettings[string.Format("Web-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("Cor-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'Cor', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Cor = System.Configuration.ConfigurationManager.AppSettings[string.Format("Cor-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("CorNacional-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'CorNacional', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                CorNacional = System.Configuration.ConfigurationManager.AppSettings[string.Format("CorNacional-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("TextoLetraCambio-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'TextoLetraCambio', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                TextoLetraCambio = System.Configuration.ConfigurationManager.AppSettings[string.Format("TextoLetraCambio-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("Observaciones-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'Observaciones', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Observaciones = System.Configuration.ConfigurationManager.AppSettings[string.Format("Observaciones-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("Regimen-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'Regimen', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Regimen = System.Configuration.ConfigurationManager.AppSettings[string.Format("Regimen-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("AutoRete-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'AutoRete', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                AutoRete = System.Configuration.ConfigurationManager.AppSettings[string.Format("AutoRete-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("ResAutorete-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'ResAutorete', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                ResAutorete = System.Configuration.ConfigurationManager.AppSettings[string.Format("ResAutorete-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("ActEconomica-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'ActEconomica', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                ActEconomica = System.Configuration.ConfigurationManager.AppSettings[string.Format("ActEconomica-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("TextoAutorete-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'TextoAutorete', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                TextoAutorete = System.Configuration.ConfigurationManager.AppSettings[string.Format("TextoAutorete-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("ResDian-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'ResDian', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                ResDian = System.Configuration.ConfigurationManager.AppSettings[string.Format("ResDian-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("PagoBanco-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'PagoBanco', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                PagoBanco = System.Configuration.ConfigurationManager.AppSettings[string.Format("PagoBanco-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("PagoCheque-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'PagoCheque', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                PagoCheque = System.Configuration.ConfigurationManager.AppSettings[string.Format("PagoCheque-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("DeclaracionFOB-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'DeclaracionFOB', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                DeclaracionFOB = System.Configuration.ConfigurationManager.AppSettings[string.Format("DeclaracionFOB-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("TextoFOB-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'TextoFOB', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                TextoFOB = System.Configuration.ConfigurationManager.AppSettings[string.Format("TextoFOB-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("Nota-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'Nota', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Nota = System.Configuration.ConfigurationManager.AppSettings[string.Format("Nota-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("EmpresaFOB-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'EmpresaFOB', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                EmpresaFOB = System.Configuration.ConfigurationManager.AppSettings[string.Format("EmpresaFOB-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("Impresion-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'Impresion', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Impresion = System.Configuration.ConfigurationManager.AppSettings[string.Format("Impresion-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("Logo-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'Logo', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Logo = "file:///" + Environment.CurrentDirectory + "\\Logos\\" + System.Configuration.ConfigurationManager.AppSettings[string.Format("Logo-{0}", Program.Empresa)];
        }

        public void CargarParametrosIngles()
        {
            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("NombreEmpresaIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'NombreEmpresaIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                NombreEmpresa = System.Configuration.ConfigurationManager.AppSettings[string.Format("NombreEmpresaIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("NitIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'NitIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Nit = System.Configuration.ConfigurationManager.AppSettings[string.Format("NitIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("DirIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'DirIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Dir = System.Configuration.ConfigurationManager.AppSettings[string.Format("DirIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("ApartadoIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'ApartadoIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Apartado = System.Configuration.ConfigurationManager.AppSettings[string.Format("ApartadoIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("TelIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'TelIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Tel = System.Configuration.ConfigurationManager.AppSettings[string.Format("TelIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("WebIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'WebIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Web = System.Configuration.ConfigurationManager.AppSettings[string.Format("WebIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("CorIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'CorIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Cor = System.Configuration.ConfigurationManager.AppSettings[string.Format("CorIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("TextoLetraCambioIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'TextoLetraCambioIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                TextoLetraCambio = System.Configuration.ConfigurationManager.AppSettings[string.Format("TextoLetraCambioIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("ObservacionesIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'ObservacionesIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Observaciones = System.Configuration.ConfigurationManager.AppSettings[string.Format("ObservacionesIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("RegimenIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'RegimenIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Regimen = System.Configuration.ConfigurationManager.AppSettings[string.Format("RegimenIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("AutoReteIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'AutoReteIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                AutoRete = System.Configuration.ConfigurationManager.AppSettings[string.Format("AutoReteIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("ResAutoreteIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'ResAutoreteIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                ResAutorete = System.Configuration.ConfigurationManager.AppSettings[string.Format("ResAutoreteIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("TextoAutoreteIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'TextoAutoreteIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                TextoAutorete = System.Configuration.ConfigurationManager.AppSettings[string.Format("TextoAutoreteIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("ResDianIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'ResDianIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                ResDian = System.Configuration.ConfigurationManager.AppSettings[string.Format("ResDianIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("PagoBancoIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'PagoBancoIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                PagoBanco = System.Configuration.ConfigurationManager.AppSettings[string.Format("PagoBancoIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("PagoChequeIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'PagoChequeIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                PagoCheque = System.Configuration.ConfigurationManager.AppSettings[string.Format("PagoChequeIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("DeclaracionFOBIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'DeclaracionFOBIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                DeclaracionFOB = System.Configuration.ConfigurationManager.AppSettings[string.Format("DeclaracionFOBIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("TextoFOBIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'TextoFOBIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                TextoFOB = System.Configuration.ConfigurationManager.AppSettings[string.Format("TextoFOBIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("NotaIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'NotaIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Nota = System.Configuration.ConfigurationManager.AppSettings[string.Format("NotaIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("EmpresaFOBIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'EmpresaFOBIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                EmpresaFOB = System.Configuration.ConfigurationManager.AppSettings[string.Format("EmpresaFOBIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("ImpresionIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'ImpresionIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Impresion = System.Configuration.ConfigurationManager.AppSettings[string.Format("ImpresionIng-{0}", Program.Empresa)];

            if (System.Configuration.ConfigurationManager.AppSettings.GetValues(string.Format("LogoIng-{0}", Program.Empresa)) == null)
                MessageBox.Show("El parametro 'LogoIng', configurable en el archivo FacturaVenta.config, ha sido borrado");
            else
                Logo = "file:///" + Environment.CurrentDirectory + "\\Logos\\" + System.Configuration.ConfigurationManager.AppSettings[string.Format("LogoIng-{0}", Program.Empresa)];
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            if (ValidarCampos())
            {
                if (chkPreview.Checked)
                {
                    if (txtConsecutivoInicial.Text != txtConsecutivoFinal.Text)
                    {
                        txtConsecutivoFinal.Text = txtConsecutivoInicial.Text;
                    }
                }
                llenarDatos();
                if (chkImprimir.Checked)
                {
                    Imprimir();
                }
                else
                {
                    Preview();
                }

            }
        }

        private void btnDocumentos_Click(object sender, EventArgs e)
        {
            List<TiposDctos> lstDocumentos = new DocumentosRepository().ListarTiposDoc();
            FrmPopUp pListarDocumentos = new FrmPopUp(lstDocumentos);
            pListarDocumentos.ShowDialog();
            if (!string.IsNullOrEmpty(pListarDocumentos.pDescripcion))
            {
                txtTipoDcto.Text = pListarDocumentos.pCodigo;
            }
            txtConsecutivoInicial.Text = new DocumentosRepository().Consecutivo(txtTipoDcto.Text);
            txtConsecutivoFinal.Text = txtConsecutivoInicial.Text;
        }

        public void Imprimir()
        {
            int intCopias = (int)nudCopias.Value;
            int Cont = 1;
            string impresora = cmbImpresoras.Text;
            foreach (string nroDcto in lstFacturaVenta.GroupBy(x => x.NroDcto).Select(y => y.FirstOrDefault().NroDcto))
            {
                Decimal? Activa = lstFacturaVenta.Where(x => x.NroDcto == nroDcto).Select(x => x.Bruto).FirstOrDefault();
                Zona = lstFacturaVenta.Where(x => x.NroDcto == nroDcto).Select(x => x.Zona).FirstOrDefault();
                Cliente = lstFacturaVenta.Where(x => x.NroDcto == nroDcto).Select(x => x.CodAlterno).FirstOrDefault();

                 while (Cont <= intCopias)
                {
                    try
                    {
                        if (chkDeProducto.Checked)
                        {
                            if (Activa > 0)
                            {
                                if (chkCopia.Checked)
                                {
                                    Copia = "Copia Cartera";
                                }
                                else if (chkCopiaCartera.Checked)
                                {
                                    if ((Zona == CodZona1 || Zona == CodZona2 || Zona == CodZona3 || Zona == CodZona4 || Zona == CodZona5 || Zona == CodZona6)
                                        && (Cliente != CodCliente1 && Cliente != CodCliente2))
                                    {
                                        if (Cont == 1)
                                        {
                                            Copia = "Original";
                                        }
                                        else if (Cont >= 2)
                                        {
                                            Copia = "Copia Cartera";
                                        }
                                    }
                                    else
                                    {
                                        if (Cont == 1)
                                        {
                                            Copia = "Original";
                                        }
                                        else if (Cont >= 2)
                                        {
                                            Copia = "Copia";
                                        }
                                    }
                                }
                                else
                                {
                                    if (Cont == 1)
                                    {
                                        Copia = "Original";
                                    }
                                    else if (Cont >= 2)
                                    {
                                        Copia = "Copia";
                                    }
                                }
                            }
                            else
                            {
                                Copia = "Anulada";
                            }
                        }
                        else
                        {
                            if (Activa > 0)
                            {
                                if (!chkCopia.Checked)
                                {
                                    if (Cont == 1)
                                    {
                                        Copia = "Original";
                                    }
                                    else if (Cont >= 2)
                                    {
                                        Copia = "Copia";
                                    }
                                }
                                else
                                {
                                    Copia = "Copia Cartera";
                                } 
                            }
                            else
                            {
                                Copia = "Anulada";
                            }
                        }

                        reporte = ParamametrosReporte();
                        reporte = CargarParametrosReporte();
                        reporte.DataSources.Add(new ReportDataSource("LstFactura", lstFacturaVenta.Where(x => x.NroDcto == nroDcto)));
                        reporte.EnableExternalImages = true;
                        Export(reporte);
                        m_currentPageIndex = 0;
                        PrintDocument printDoc = new PrintDocument();
                        printDoc.PrinterSettings.PrinterName = impresora;
                        printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                        printDoc.Print();

                        Cont++;
                    }

                    catch (Exception er)
                    {
                        MessageBox.Show("Ocurrió un error al imprimir la factura " + er, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }


                };
                Cont = 1;
            }

        }

        public void Preview()
        {
            frmPrev = new FrmPreview(this);
            RptPrev = frmPrev.PreviewReport.LocalReport;

            foreach (string nroDcto in lstFacturaVenta.GroupBy(x => x.NroDcto).Select(y => y.FirstOrDefault().NroDcto))
            {

                RptPrev = ParamametrosReporte();
                RptPrev = CargarParametrosReporte();
                RptPrev.DataSources.Add(new ReportDataSource("LstFactura", lstFacturaVenta.Where(x => x.NroDcto == nroDcto)));

                frmPrev.PreviewReport.SetDisplayMode(DisplayMode.PrintLayout);
                frmPrev.PreviewReport.RefreshReport();
                frmPrev.ShowDialog();
            }


        }

        public void llenarDatos()
        {
            Palabrauno = txtPalabraUno.Text;
            PalabraDos = txtPalabraDos.Text;
            PalabraTres = txtPalabraTres.Text;
            PalabraCuatro = txtPalabraCuatro.Text;
            DocInicial = txtConsecutivoInicial.Text;
            DocFinal = txtConsecutivoFinal.Text;
            TipoDcto = txtTipoDcto.Text;
            CodZona1 = txtZonaUno.Text;
            CodZona2 = txtZonaDos.Text;
            CodZona3 = txtZonaTres.Text;
            CodZona4 = txtZonaCuatro.Text;
            CodZona5 = txtZonaCinco.Text;
            CodZona6 = txtZonaSeis.Text;
            CodCliente1 = txtClienteUno.Text;
            CodCliente2 = txtClienteDos.Text;

            //Consulta de la factura de venta
            DataTable dtbl = new DataTable();
            dtbl = new FacturaRepository().ConsultaDeFacturaVenta(DocInicial, DocFinal, TipoDcto, Palabrauno, PalabraDos, PalabraTres, PalabraCuatro);
            lstFacturaVenta = DatatableToList(dtbl);

        }

        private LocalReport ParamametrosReporte()
        {
            if (chkFacComposicion.Checked)
            {
                if (chkIdiomaEspanol.Checked)
                {
                    if (chkImprimir.Checked)
                    {
                        LocalReport reporte = new LocalReport();
                        reporte.ReportEmbeddedResource = "FacturaVenta.rptFacturaComposicion.rdlc";
                        reporte.EnableExternalImages = true;
                        CargarParametros();
                        return reporte;
                    }
                    else
                    {
                        RptPrev.ReportEmbeddedResource = "FacturaVenta.rptFacturaComposicion.rdlc";
                        RptPrev.EnableExternalImages = true;
                        CargarParametros();
                        return RptPrev;
                    }
                }
                else
                {
                    if (chkImprimir.Checked)
                    {
                        LocalReport reporte = new LocalReport();
                        reporte.ReportEmbeddedResource = "FacturaVenta.rptFacturaComposicionIngles.rdlc";
                        reporte.EnableExternalImages = true;
                        CargarParametrosIngles();
                        return reporte;
                    }
                    else
                    {
                        RptPrev.ReportEmbeddedResource = "FacturaVenta.rptFacturaComposicionIngles.rdlc";
                        RptPrev.EnableExternalImages = true;
                        CargarParametrosIngles();
                        return RptPrev;
                    }
                }
            }
            else
            {
                if (chkImprimir.Checked)
                {
                    if (chkFactProductoPesos.Checked)
                    {
                        LocalReport reporte = new LocalReport();
                        reporte.ReportEmbeddedResource = "FacturaVenta.rptFacturaProducto.rdlc";
                        reporte.EnableExternalImages = true;
                        CargarParametros();
                        return reporte;
                    }
                    else
                    {
                        LocalReport reporte = new LocalReport();
                        reporte.ReportEmbeddedResource = "FacturaVenta.rptFacturaProductoDolares.rdlc";
                        reporte.EnableExternalImages = true;
                        CargarParametros();
                        return reporte;
                    }
                }
                else
                {
                    if (chkFactProductoPesos.Checked)
                    {
                        RptPrev.ReportEmbeddedResource = "FacturaVenta.rptFacturaProducto.rdlc";
                        RptPrev.EnableExternalImages = true;
                        CargarParametros();
                        return RptPrev;
                    }
                    else
                    {
                        RptPrev.ReportEmbeddedResource = "FacturaVenta.rptFacturaProductoDolares.rdlc";
                        RptPrev.EnableExternalImages = true;
                        CargarParametros();
                        return RptPrev;
                    }
                }
            }

        }

        private LocalReport CargarParametrosReporte()
        {
            ReportParameter param;
            ReportParameter[] params1;

            params1 = new ReportParameter[25];
            param = null;

            param = new ReportParameter("NombreEmpresa", NombreEmpresa);
            params1[0] = param;

            param = new ReportParameter("Nit", Nit);
            params1[1] = param;

            param = new ReportParameter("Dir", Dir);
            params1[2] = param;

            param = new ReportParameter("Apartado", Apartado);
            params1[3] = param;

            param = new ReportParameter("Tel", Tel);
            params1[4] = param;

            param = new ReportParameter("Web", Web);
            params1[5] = param;

            param = new ReportParameter("Cor", Cor);
            params1[6] = param;

            param = new ReportParameter("TextoLetraCambio", TextoLetraCambio);
            params1[7] = param;

            param = new ReportParameter("Observaciones", Observaciones);
            params1[8] = param;

            param = new ReportParameter("Regimen", Regimen);
            params1[9] = param;

            param = new ReportParameter("AutoRete", AutoRete);
            params1[10] = param;

            param = new ReportParameter("ResAutorete", ResAutorete);
            params1[11] = param;

            param = new ReportParameter("TextoAutorete", TextoAutorete);
            params1[12] = param;

            param = new ReportParameter("ResDian", ResDian);
            params1[13] = param;

            param = new ReportParameter("PagoBanco", PagoBanco);
            params1[14] = param;

            param = new ReportParameter("PagoCheque", PagoCheque);
            params1[15] = param;

            param = new ReportParameter("DeclaracionFOB", DeclaracionFOB);
            params1[16] = param;

            param = new ReportParameter("TextoFOB", TextoFOB);
            params1[17] = param;

            param = new ReportParameter("EmpresaFOB", EmpresaFOB);
            params1[18] = param;

            param = new ReportParameter("Logo", Logo);
            params1[19] = param;

            param = new ReportParameter("Nota", Nota);
            params1[20] = param;

            param = new ReportParameter("CorNacional", CorNacional);
            params1[21] = param;

            param = new ReportParameter("ActEconomica", ActEconomica);
            params1[22] = param;

            param = new ReportParameter("Copia", Copia);
            params1[23] = param;

            param = new ReportParameter("Impresion", Impresion);
            params1[24] = param;

            if (chkImprimir.Checked)
            {
                reporte.SetParameters(params1);
                return reporte;
            }
            else
            {
                RptPrev.SetParameters(params1);
                return RptPrev;
            }
        }

        public List<Factura> DatatableToList(DataTable dt)
        {
            List<Factura> lstFactura = new List<Factura>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Factura EntidadFac;
                for (int i = 0; i < rowsCount; i++)
                {
                    EntidadFac = new Factura();

                    EntidadFac.NroDcto = dt.Rows[i]["NroDcto"].ToString().Trim();
                    EntidadFac.Fecha = (DateTime)dt.Rows[i]["FechaFac"];
                    EntidadFac.FechaVence = (DateTime)dt.Rows[i]["FechaVencimiento"];
                    EntidadFac.Nombre = dt.Rows[i]["NombreCliente"].ToString().Trim().Trim();
                    EntidadFac.Nit = dt.Rows[i]["Nit"].ToString().Trim();
                    EntidadFac.Direccion = dt.Rows[i]["DireccionCliente"].ToString().Trim().Trim().ToLower();
                    EntidadFac.DireccionAlterna = dt.Rows[i]["DireccionAlterna"].ToString().Trim().ToLower();
                    EntidadFac.Pais = dt.Rows[i]["PaisCliente"].ToString().Trim().Trim();
                    EntidadFac.Ciudad = dt.Rows[i]["CiudadCliente"].ToString().Trim().Trim();
                    EntidadFac.Telefono = dt.Rows[i]["TelefonoCliente"].ToString().Trim();
                    EntidadFac.CodigoCliente = dt.Rows[i]["CodigoCliente"].ToString().Trim();
                    EntidadFac.NroPedido = dt.Rows[i]["NroPedido"].ToString().Trim();
                    EntidadFac.NroOrden = dt.Rows[i]["NumeroOrden"].ToString().Trim();
                    EntidadFac.Zona = dt.Rows[i]["ZonaCliente"].ToString().Trim();
                    EntidadFac.Transportador = dt.Rows[i]["Transportador"].ToString().Trim();
                    EntidadFac.Vendedor = dt.Rows[i]["CodigoVendedor"].ToString().Trim();
                    EntidadFac.CondicionesPago = dt.Rows[i]["CondicionPago"].ToString().Trim();
                    EntidadFac.CorreoCliente = dt.Rows[i]["CorreoCliente"].ToString().Trim();
                    EntidadFac.Referencia = dt.Rows[i]["Referencia"].ToString().Trim();
                    EntidadFac.Descripcion = dt.Rows[i]["Descripcion"].ToString().Trim();
                    EntidadFac.DescripcionCorta = dt.Rows[i]["DescripcionCorta"].ToString().Trim();
                    EntidadFac.Cantidad = (decimal)dt.Rows[i]["Cantidad"];
                    EntidadFac.UnidadMedida = dt.Rows[i]["UnidadMedida"].ToString().Trim();
                    EntidadFac.VlrUnit = (decimal)dt.Rows[i]["ValorVenta"];
                    EntidadFac.VlrUnitTotal = (decimal)dt.Rows[i]["Total"];
                    EntidadFac.Agrupacion = dt.Rows[i]["Agrupacion"].ToString().Trim();
                    EntidadFac.Bruto = (decimal)dt.Rows[i]["TotalBruto"];
                    EntidadFac.BrutoDolar = (decimal)dt.Rows[i]["TotalBrutoDolar"];
                    EntidadFac.Descuento = (decimal)dt.Rows[i]["Descuento"];
                    EntidadFac.Iva = (decimal)dt.Rows[i]["Iva"];
                    EntidadFac.VlrNetoPagar = (decimal)dt.Rows[i]["NetoPagar"];
                    EntidadFac.PesoNeto = (decimal)dt.Rows[i]["PesoNeto"];
                    EntidadFac.PesoBruto = (decimal)dt.Rows[i]["PesoBruto"];
                    EntidadFac.TotalCajas = (int)dt.Rows[i]["TotalCajas"];
                    EntidadFac.LugarFob = dt.Rows[i]["LugarFob"].ToString().Trim();
                    EntidadFac.DsctoComercial = dt.Rows[i]["DescuentoComercial"].ToString().Trim();
                    EntidadFac.Nota = dt.Rows[i]["Nota"].ToString().Trim();
                    EntidadFac.Activo = (bool)dt.Rows[i]["Activa"];
                    EntidadFac.CodAlterno = dt.Rows[i]["CodAlterno"].ToString().Trim();
                    EntidadFac.FechaFob = System.DateTime.Now.ToString("dd-MM-yyyy");
                    lstFactura.Add(EntidadFac);

                }
            }
            return lstFactura;
        }

        #endregion

        #region Impresion

        public void Dispose()
        {
            if (m_streams != null)
            {
                foreach (Stream stream in m_streams)
                    stream.Close();
                m_streams = null;
            }
        }

        private Stream CreateStream(string name, string fileNameExtension, Encoding encoding, string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            return stream;
        }
        // Export the given report as an EMF (Enhanced Metafile) file.
        private void Export(LocalReport report)
        {
            string deviceInfo = "<DeviceInfo>" + " <OutputFormat>EMF</OutputFormat></DeviceInfo>";
            Warning[] warnings;
            m_streams = new List<Stream>();
            report.Render("Image", deviceInfo, CreateStream, out warnings);
            foreach (Stream stream in m_streams)
                stream.Position = 0;
        }
        // Handler for PrintPageEvents
        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            Metafile pageImage = new Metafile(m_streams[m_currentPageIndex]);
            ev.Graphics.DrawImage(pageImage, ev.PageBounds);

            m_currentPageIndex++;
            ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
        }

        private void Print(short pages)
        {
            if (m_streams == null || m_streams.Count == 0)
                throw new Exception("Error: no stream to print.");
            PrintDocument printDoc = new PrintDocument();
            printDoc.PrinterSettings.Copies = pages;
            if (!printDoc.PrinterSettings.IsValid)
            {
                throw new Exception("Error: cannot find the default printer.");
            }
            else
            {
                printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                m_currentPageIndex = 0;
                printDoc.Print();
            }
        }


        #endregion


    }
}
