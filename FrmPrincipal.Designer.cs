﻿namespace FacturaVenta
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.grbEncabezado = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkCopiaCartera = new System.Windows.Forms.CheckBox();
            this.chkCopia = new System.Windows.Forms.CheckBox();
            this.btnDocumentos = new System.Windows.Forms.Button();
            this.txtConsecutivoFinal = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtConsecutivoInicial = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTipoDcto = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkDeProducto = new System.Windows.Forms.CheckBox();
            this.chkFacComposicion = new System.Windows.Forms.CheckBox();
            this.grbParametrosComposicion = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPalabraCuatro = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPalabraTres = new System.Windows.Forms.TextBox();
            this.txtPalabraDos = new System.Windows.Forms.TextBox();
            this.txtPalabraUno = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkIdiomaEspanol = new System.Windows.Forms.CheckBox();
            this.chkIdiomaIngles = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chkPreview = new System.Windows.Forms.CheckBox();
            this.chkImprimir = new System.Windows.Forms.CheckBox();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.nudCopias = new System.Windows.Forms.NumericUpDown();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.cmbImpresoras = new System.Windows.Forms.ComboBox();
            this.PrintPreviewDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.PrintDocument = new System.Drawing.Printing.PrintDocument();
            this.grbParamFactPorducto = new System.Windows.Forms.GroupBox();
            this.chkFacProductoDolares = new System.Windows.Forms.CheckBox();
            this.chkFactProductoPesos = new System.Windows.Forms.CheckBox();
            this.grbParametrosCopiaCartera = new System.Windows.Forms.GroupBox();
            this.txtZonaSeis = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtZonaCinco = new System.Windows.Forms.TextBox();
            this.lblzona5 = new System.Windows.Forms.Label();
            this.txtClienteDos = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtClienteUno = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtZonaCuatro = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtZonaTres = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtZonaDos = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtZonaUno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grbEncabezado.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.grbParametrosComposicion.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCopias)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.grbParamFactPorducto.SuspendLayout();
            this.grbParametrosCopiaCartera.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbEncabezado
            // 
            this.grbEncabezado.Controls.Add(this.pictureBox1);
            this.grbEncabezado.Controls.Add(this.label1);
            this.grbEncabezado.Controls.Add(this.lblTitulo);
            this.grbEncabezado.Location = new System.Drawing.Point(3, 1);
            this.grbEncabezado.Name = "grbEncabezado";
            this.grbEncabezado.Size = new System.Drawing.Size(489, 114);
            this.grbEncabezado.TabIndex = 0;
            this.grbEncabezado.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(10, 18);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(185, 79);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(242, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(198, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Facturas de Venta";
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblTitulo.Location = new System.Drawing.Point(198, 25);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(287, 31);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Impresión de Formatos";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkCopiaCartera);
            this.groupBox1.Controls.Add(this.chkCopia);
            this.groupBox1.Controls.Add(this.btnDocumentos);
            this.groupBox1.Controls.Add(this.txtConsecutivoFinal);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtConsecutivoInicial);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtTipoDcto);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.chkDeProducto);
            this.groupBox1.Controls.Add(this.chkFacComposicion);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 119);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(489, 105);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selección de tipo de factura";
            // 
            // chkCopiaCartera
            // 
            this.chkCopiaCartera.AutoSize = true;
            this.chkCopiaCartera.Location = new System.Drawing.Point(9, 80);
            this.chkCopiaCartera.Name = "chkCopiaCartera";
            this.chkCopiaCartera.Size = new System.Drawing.Size(144, 19);
            this.chkCopiaCartera.TabIndex = 11;
            this.chkCopiaCartera.Text = "Copia Cliente Cartera";
            this.chkCopiaCartera.UseVisualStyleBackColor = true;
            this.chkCopiaCartera.CheckedChanged += new System.EventHandler(this.chkCopiaCartera_CheckedChanged);
            // 
            // chkCopia
            // 
            this.chkCopia.AutoSize = true;
            this.chkCopia.Location = new System.Drawing.Point(9, 61);
            this.chkCopia.Name = "chkCopia";
            this.chkCopia.Size = new System.Drawing.Size(101, 19);
            this.chkCopia.TabIndex = 3;
            this.chkCopia.Text = "Copia Cartera";
            this.chkCopia.UseVisualStyleBackColor = true;
            this.chkCopia.CheckedChanged += new System.EventHandler(this.chkCopia_CheckedChanged);
            // 
            // btnDocumentos
            // 
            this.btnDocumentos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDocumentos.BackgroundImage")));
            this.btnDocumentos.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDocumentos.Location = new System.Drawing.Point(382, 11);
            this.btnDocumentos.Name = "btnDocumentos";
            this.btnDocumentos.Size = new System.Drawing.Size(33, 29);
            this.btnDocumentos.TabIndex = 10;
            this.btnDocumentos.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDocumentos.UseVisualStyleBackColor = true;
            this.btnDocumentos.Click += new System.EventHandler(this.btnDocumentos_Click);
            // 
            // txtConsecutivoFinal
            // 
            this.txtConsecutivoFinal.Location = new System.Drawing.Point(326, 72);
            this.txtConsecutivoFinal.Name = "txtConsecutivoFinal";
            this.txtConsecutivoFinal.Size = new System.Drawing.Size(134, 21);
            this.txtConsecutivoFinal.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(169, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Número Documento Final";
            // 
            // txtConsecutivoInicial
            // 
            this.txtConsecutivoInicial.Location = new System.Drawing.Point(326, 43);
            this.txtConsecutivoInicial.Name = "txtConsecutivoInicial";
            this.txtConsecutivoInicial.Size = new System.Drawing.Size(134, 21);
            this.txtConsecutivoInicial.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(169, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(150, 15);
            this.label8.TabIndex = 6;
            this.label8.Text = "Número Documento Inicial\r\n";
            // 
            // txtTipoDcto
            // 
            this.txtTipoDcto.Location = new System.Drawing.Point(326, 14);
            this.txtTipoDcto.Name = "txtTipoDcto";
            this.txtTipoDcto.ReadOnly = true;
            this.txtTipoDcto.Size = new System.Drawing.Size(48, 21);
            this.txtTipoDcto.TabIndex = 5;
            this.txtTipoDcto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(169, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 15);
            this.label7.TabIndex = 4;
            this.label7.Text = "Tipo Documento";
            // 
            // chkDeProducto
            // 
            this.chkDeProducto.AutoSize = true;
            this.chkDeProducto.Location = new System.Drawing.Point(9, 42);
            this.chkDeProducto.Name = "chkDeProducto";
            this.chkDeProducto.Size = new System.Drawing.Size(90, 19);
            this.chkDeProducto.TabIndex = 3;
            this.chkDeProducto.Text = "De Producto";
            this.chkDeProducto.UseVisualStyleBackColor = true;
            this.chkDeProducto.CheckedChanged += new System.EventHandler(this.chkDeProducto_CheckedChanged);
            // 
            // chkFacComposicion
            // 
            this.chkFacComposicion.AutoSize = true;
            this.chkFacComposicion.Location = new System.Drawing.Point(9, 23);
            this.chkFacComposicion.Name = "chkFacComposicion";
            this.chkFacComposicion.Size = new System.Drawing.Size(112, 19);
            this.chkFacComposicion.TabIndex = 2;
            this.chkFacComposicion.Text = "De Composición";
            this.chkFacComposicion.UseVisualStyleBackColor = true;
            this.chkFacComposicion.CheckedChanged += new System.EventHandler(this.chkFacComposicion_CheckedChanged);
            // 
            // grbParametrosComposicion
            // 
            this.grbParametrosComposicion.Controls.Add(this.groupBox5);
            this.grbParametrosComposicion.Controls.Add(this.groupBox2);
            this.grbParametrosComposicion.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbParametrosComposicion.Location = new System.Drawing.Point(3, 409);
            this.grbParametrosComposicion.Name = "grbParametrosComposicion";
            this.grbParametrosComposicion.Size = new System.Drawing.Size(488, 185);
            this.grbParametrosComposicion.TabIndex = 2;
            this.grbParametrosComposicion.TabStop = false;
            this.grbParametrosComposicion.Text = "Parámetros Factura de Composición";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.txtPalabraCuatro);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.txtPalabraTres);
            this.groupBox5.Controls.Add(this.txtPalabraDos);
            this.groupBox5.Controls.Add(this.txtPalabraUno);
            this.groupBox5.Location = new System.Drawing.Point(135, 29);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(338, 147);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Apartir de las siguientes palabras excluir de la descripción";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "4";
            // 
            // txtPalabraCuatro
            // 
            this.txtPalabraCuatro.Location = new System.Drawing.Point(33, 120);
            this.txtPalabraCuatro.Name = "txtPalabraCuatro";
            this.txtPalabraCuatro.Size = new System.Drawing.Size(226, 21);
            this.txtPalabraCuatro.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "1";
            // 
            // txtPalabraTres
            // 
            this.txtPalabraTres.Location = new System.Drawing.Point(33, 85);
            this.txtPalabraTres.Name = "txtPalabraTres";
            this.txtPalabraTres.Size = new System.Drawing.Size(226, 21);
            this.txtPalabraTres.TabIndex = 2;
            // 
            // txtPalabraDos
            // 
            this.txtPalabraDos.Location = new System.Drawing.Point(33, 53);
            this.txtPalabraDos.Name = "txtPalabraDos";
            this.txtPalabraDos.Size = new System.Drawing.Size(226, 21);
            this.txtPalabraDos.TabIndex = 1;
            // 
            // txtPalabraUno
            // 
            this.txtPalabraUno.Location = new System.Drawing.Point(33, 21);
            this.txtPalabraUno.Name = "txtPalabraUno";
            this.txtPalabraUno.Size = new System.Drawing.Size(226, 21);
            this.txtPalabraUno.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkIdiomaEspanol);
            this.groupBox2.Controls.Add(this.chkIdiomaIngles);
            this.groupBox2.Location = new System.Drawing.Point(9, 55);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(120, 89);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Idioma";
            // 
            // chkIdiomaEspanol
            // 
            this.chkIdiomaEspanol.AutoSize = true;
            this.chkIdiomaEspanol.Location = new System.Drawing.Point(23, 28);
            this.chkIdiomaEspanol.Name = "chkIdiomaEspanol";
            this.chkIdiomaEspanol.Size = new System.Drawing.Size(67, 19);
            this.chkIdiomaEspanol.TabIndex = 1;
            this.chkIdiomaEspanol.Text = "Español";
            this.chkIdiomaEspanol.UseVisualStyleBackColor = true;
            this.chkIdiomaEspanol.CheckedChanged += new System.EventHandler(this.chkIdiomaEspanol_CheckedChanged);
            // 
            // chkIdiomaIngles
            // 
            this.chkIdiomaIngles.AutoSize = true;
            this.chkIdiomaIngles.Location = new System.Drawing.Point(23, 53);
            this.chkIdiomaIngles.Name = "chkIdiomaIngles";
            this.chkIdiomaIngles.Size = new System.Drawing.Size(58, 19);
            this.chkIdiomaIngles.TabIndex = 2;
            this.chkIdiomaIngles.Text = "Ingles";
            this.chkIdiomaIngles.UseVisualStyleBackColor = true;
            this.chkIdiomaIngles.CheckedChanged += new System.EventHandler(this.chkIdiomaIngles_CheckedChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox3);
            this.groupBox6.Controls.Add(this.btnImprimir);
            this.groupBox6.Controls.Add(this.groupBox8);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(3, 596);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(488, 121);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Impresión";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chkPreview);
            this.groupBox3.Controls.Add(this.chkImprimir);
            this.groupBox3.Location = new System.Drawing.Point(9, 68);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(351, 46);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Acción";
            // 
            // chkPreview
            // 
            this.chkPreview.AutoSize = true;
            this.chkPreview.Location = new System.Drawing.Point(184, 17);
            this.chkPreview.Name = "chkPreview";
            this.chkPreview.Size = new System.Drawing.Size(117, 19);
            this.chkPreview.TabIndex = 1;
            this.chkPreview.Text = "Previsualización";
            this.chkPreview.UseVisualStyleBackColor = true;
            this.chkPreview.CheckedChanged += new System.EventHandler(this.chkPreview_CheckedChanged);
            // 
            // chkImprimir
            // 
            this.chkImprimir.AutoSize = true;
            this.chkImprimir.Location = new System.Drawing.Point(60, 17);
            this.chkImprimir.Name = "chkImprimir";
            this.chkImprimir.Size = new System.Drawing.Size(76, 19);
            this.chkImprimir.TabIndex = 0;
            this.chkImprimir.Text = "Imprimir";
            this.chkImprimir.UseVisualStyleBackColor = true;
            this.chkImprimir.CheckedChanged += new System.EventHandler(this.chkImprimir_CheckedChanged);
            // 
            // btnImprimir
            // 
            this.btnImprimir.Location = new System.Drawing.Point(366, 71);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(116, 43);
            this.btnImprimir.TabIndex = 2;
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.nudCopias);
            this.groupBox8.Location = new System.Drawing.Point(366, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(114, 53);
            this.groupBox8.TabIndex = 1;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Cantidad Copias";
            // 
            // nudCopias
            // 
            this.nudCopias.Location = new System.Drawing.Point(14, 21);
            this.nudCopias.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudCopias.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCopias.Name = "nudCopias";
            this.nudCopias.Size = new System.Drawing.Size(67, 22);
            this.nudCopias.TabIndex = 0;
            this.nudCopias.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudCopias.ThousandsSeparator = true;
            this.nudCopias.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.cmbImpresoras);
            this.groupBox7.Location = new System.Drawing.Point(9, 12);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(351, 53);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Seleccione una Impresora";
            // 
            // cmbImpresoras
            // 
            this.cmbImpresoras.FormattingEnabled = true;
            this.cmbImpresoras.Location = new System.Drawing.Point(6, 21);
            this.cmbImpresoras.Name = "cmbImpresoras";
            this.cmbImpresoras.Size = new System.Drawing.Size(339, 23);
            this.cmbImpresoras.TabIndex = 0;
            // 
            // PrintPreviewDialog
            // 
            this.PrintPreviewDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.PrintPreviewDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.PrintPreviewDialog.ClientSize = new System.Drawing.Size(400, 300);
            this.PrintPreviewDialog.Enabled = true;
            this.PrintPreviewDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("PrintPreviewDialog.Icon")));
            this.PrintPreviewDialog.Name = "PrintPreviewDialog";
            this.PrintPreviewDialog.Visible = false;
            // 
            // grbParamFactPorducto
            // 
            this.grbParamFactPorducto.Controls.Add(this.chkFacProductoDolares);
            this.grbParamFactPorducto.Controls.Add(this.chkFactProductoPesos);
            this.grbParamFactPorducto.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbParamFactPorducto.Location = new System.Drawing.Point(3, 353);
            this.grbParamFactPorducto.Name = "grbParamFactPorducto";
            this.grbParamFactPorducto.Size = new System.Drawing.Size(489, 52);
            this.grbParamFactPorducto.TabIndex = 4;
            this.grbParamFactPorducto.TabStop = false;
            this.grbParamFactPorducto.Text = "Parámetros Factura de Producto";
            // 
            // chkFacProductoDolares
            // 
            this.chkFacProductoDolares.AutoSize = true;
            this.chkFacProductoDolares.Location = new System.Drawing.Point(288, 17);
            this.chkFacProductoDolares.Name = "chkFacProductoDolares";
            this.chkFacProductoDolares.Size = new System.Drawing.Size(84, 19);
            this.chkFacProductoDolares.TabIndex = 1;
            this.chkFacProductoDolares.Text = "En Dolares";
            this.chkFacProductoDolares.UseVisualStyleBackColor = true;
            this.chkFacProductoDolares.CheckedChanged += new System.EventHandler(this.chkFacProductoDolares_CheckedChanged);
            // 
            // chkFactProductoPesos
            // 
            this.chkFactProductoPesos.AutoSize = true;
            this.chkFactProductoPesos.Location = new System.Drawing.Point(116, 17);
            this.chkFactProductoPesos.Name = "chkFactProductoPesos";
            this.chkFactProductoPesos.Size = new System.Drawing.Size(72, 19);
            this.chkFactProductoPesos.TabIndex = 0;
            this.chkFactProductoPesos.Text = "En Pesos";
            this.chkFactProductoPesos.UseVisualStyleBackColor = true;
            this.chkFactProductoPesos.CheckedChanged += new System.EventHandler(this.chkFactProductoPesos_CheckedChanged);
            // 
            // grbParametrosCopiaCartera
            // 
            this.grbParametrosCopiaCartera.Controls.Add(this.txtZonaSeis);
            this.grbParametrosCopiaCartera.Controls.Add(this.label15);
            this.grbParametrosCopiaCartera.Controls.Add(this.txtZonaCinco);
            this.grbParametrosCopiaCartera.Controls.Add(this.lblzona5);
            this.grbParametrosCopiaCartera.Controls.Add(this.txtClienteDos);
            this.grbParametrosCopiaCartera.Controls.Add(this.label13);
            this.grbParametrosCopiaCartera.Controls.Add(this.txtClienteUno);
            this.grbParametrosCopiaCartera.Controls.Add(this.label14);
            this.grbParametrosCopiaCartera.Controls.Add(this.txtZonaCuatro);
            this.grbParametrosCopiaCartera.Controls.Add(this.label12);
            this.grbParametrosCopiaCartera.Controls.Add(this.txtZonaTres);
            this.grbParametrosCopiaCartera.Controls.Add(this.label11);
            this.grbParametrosCopiaCartera.Controls.Add(this.txtZonaDos);
            this.grbParametrosCopiaCartera.Controls.Add(this.label10);
            this.grbParametrosCopiaCartera.Controls.Add(this.txtZonaUno);
            this.grbParametrosCopiaCartera.Controls.Add(this.label3);
            this.grbParametrosCopiaCartera.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbParametrosCopiaCartera.Location = new System.Drawing.Point(3, 226);
            this.grbParametrosCopiaCartera.Name = "grbParametrosCopiaCartera";
            this.grbParametrosCopiaCartera.Size = new System.Drawing.Size(489, 124);
            this.grbParametrosCopiaCartera.TabIndex = 5;
            this.grbParametrosCopiaCartera.TabStop = false;
            this.grbParametrosCopiaCartera.Text = "Parámetros Copia de Cartera";
            // 
            // txtZonaSeis
            // 
            this.txtZonaSeis.Location = new System.Drawing.Point(270, 45);
            this.txtZonaSeis.Name = "txtZonaSeis";
            this.txtZonaSeis.Size = new System.Drawing.Size(102, 21);
            this.txtZonaSeis.TabIndex = 15;
            this.txtZonaSeis.Text = "02P";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(206, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 15);
            this.label15.TabIndex = 14;
            this.label15.Text = "Zona 6";
            // 
            // txtZonaCinco
            // 
            this.txtZonaCinco.Location = new System.Drawing.Point(270, 20);
            this.txtZonaCinco.Name = "txtZonaCinco";
            this.txtZonaCinco.Size = new System.Drawing.Size(102, 21);
            this.txtZonaCinco.TabIndex = 13;
            this.txtZonaCinco.Text = "05C";
            // 
            // lblzona5
            // 
            this.lblzona5.AutoSize = true;
            this.lblzona5.Location = new System.Drawing.Point(206, 23);
            this.lblzona5.Name = "lblzona5";
            this.lblzona5.Size = new System.Drawing.Size(42, 15);
            this.lblzona5.TabIndex = 12;
            this.lblzona5.Text = "Zona 5";
            // 
            // txtClienteDos
            // 
            this.txtClienteDos.Location = new System.Drawing.Point(270, 95);
            this.txtClienteDos.Name = "txtClienteDos";
            this.txtClienteDos.Size = new System.Drawing.Size(127, 21);
            this.txtClienteDos.TabIndex = 11;
            this.txtClienteDos.Text = "900155107-1";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(206, 98);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 15);
            this.label13.TabIndex = 10;
            this.label13.Text = "Cliente 2";
            // 
            // txtClienteUno
            // 
            this.txtClienteUno.Location = new System.Drawing.Point(270, 70);
            this.txtClienteUno.Name = "txtClienteUno";
            this.txtClienteUno.Size = new System.Drawing.Size(127, 21);
            this.txtClienteUno.TabIndex = 9;
            this.txtClienteUno.Text = "890900608-9";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(206, 73);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 15);
            this.label14.TabIndex = 8;
            this.label14.Text = "Cliente 1";
            // 
            // txtZonaCuatro
            // 
            this.txtZonaCuatro.Location = new System.Drawing.Point(56, 96);
            this.txtZonaCuatro.Name = "txtZonaCuatro";
            this.txtZonaCuatro.Size = new System.Drawing.Size(100, 21);
            this.txtZonaCuatro.TabIndex = 7;
            this.txtZonaCuatro.Text = "08A";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 99);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 15);
            this.label12.TabIndex = 6;
            this.label12.Text = "Zona 4";
            // 
            // txtZonaTres
            // 
            this.txtZonaTres.Location = new System.Drawing.Point(56, 69);
            this.txtZonaTres.Name = "txtZonaTres";
            this.txtZonaTres.Size = new System.Drawing.Size(100, 21);
            this.txtZonaTres.TabIndex = 5;
            this.txtZonaTres.Text = "06V";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 72);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 15);
            this.label11.TabIndex = 4;
            this.label11.Text = "Zona 3";
            // 
            // txtZonaDos
            // 
            this.txtZonaDos.Location = new System.Drawing.Point(56, 44);
            this.txtZonaDos.Name = "txtZonaDos";
            this.txtZonaDos.Size = new System.Drawing.Size(100, 21);
            this.txtZonaDos.TabIndex = 3;
            this.txtZonaDos.Text = "17V";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 15);
            this.label10.TabIndex = 2;
            this.label10.Text = "Zona 2";
            // 
            // txtZonaUno
            // 
            this.txtZonaUno.Location = new System.Drawing.Point(56, 20);
            this.txtZonaUno.Name = "txtZonaUno";
            this.txtZonaUno.Size = new System.Drawing.Size(100, 21);
            this.txtZonaUno.TabIndex = 1;
            this.txtZonaUno.Text = "041";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Zona 1";
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(499, 721);
            this.Controls.Add(this.grbParametrosCopiaCartera);
            this.Controls.Add(this.grbParamFactPorducto);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.grbParametrosComposicion);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grbEncabezado);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(515, 760);
            this.MinimumSize = new System.Drawing.Size(515, 726);
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Impresión de Formatos";
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.grbEncabezado.ResumeLayout(false);
            this.grbEncabezado.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grbParametrosComposicion.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudCopias)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.grbParamFactPorducto.ResumeLayout(false);
            this.grbParamFactPorducto.PerformLayout();
            this.grbParametrosCopiaCartera.ResumeLayout(false);
            this.grbParametrosCopiaCartera.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbEncabezado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkDeProducto;
        private System.Windows.Forms.CheckBox chkFacComposicion;
        private System.Windows.Forms.GroupBox grbParametrosComposicion;
        private System.Windows.Forms.CheckBox chkIdiomaIngles;
        private System.Windows.Forms.CheckBox chkIdiomaEspanol;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtConsecutivoInicial;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTipoDcto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtConsecutivoFinal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.NumericUpDown nudCopias;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox cmbImpresoras;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnDocumentos;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPalabraTres;
        private System.Windows.Forms.TextBox txtPalabraDos;
        private System.Windows.Forms.TextBox txtPalabraUno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPalabraCuatro;
        private System.Windows.Forms.CheckBox chkCopia;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox chkPreview;
        private System.Windows.Forms.CheckBox chkImprimir;
        private System.Windows.Forms.PrintPreviewDialog PrintPreviewDialog;
        private System.Drawing.Printing.PrintDocument PrintDocument;
        private System.Windows.Forms.GroupBox grbParamFactPorducto;
        private System.Windows.Forms.CheckBox chkFacProductoDolares;
        private System.Windows.Forms.CheckBox chkFactProductoPesos;
        private System.Windows.Forms.CheckBox chkCopiaCartera;
        private System.Windows.Forms.GroupBox grbParametrosCopiaCartera;
        private System.Windows.Forms.TextBox txtZonaCuatro;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtZonaTres;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtZonaDos;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtZonaUno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtClienteDos;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtClienteUno;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtZonaSeis;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtZonaCinco;
        private System.Windows.Forms.Label lblzona5;
    }
}

